import math

def prime (n):
    # 2 is the only even number that is prime
    if n==2:
          return True
     
    elif n<2 or n%2==0:   #all numbers less than 2 are not prime
          return False
     
    #  loop through the range from 3 to the sqrt of n
     
    for i in range(3,int(math.sqrt(n))+1,2):
         
         if n%i==0:
              return False
    

    return True


print(prime(7)) #returns true

     